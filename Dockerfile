FROM centos:latest
#USER root

RUN yum install python net-tools wget unzip zip java git iperf -y && \
    cd / && \
    wget http://mirrors.muzzy.it/apache/tomcat/tomcat-8/v8.0.35/bin/apache-tomcat-8.0.35.tar.gz && \
    tar zxvf apache-tomcat-8.0.35.tar.gz && \
    rm -f /apache-tomcat-8.0.35.tar.gz && \
    wget https://bootstrap.pypa.io/ez_setup.py -O - | python && \
    git clone git://github.com/feross/SpoofMAC.git && \
    chmod -R 777 /SpoofMAC && \
    cd SpoofMAC && \
    python setup.py install && \
    echo "spoof-mac.py set 02:42:0a:01:01:22 `ifconfig -a |grep eth |awk -F":" '{ print $1 }'`" >> /etc/rc.d/rc.local

#MAC-ADDRESS 02:42:0a:01:01:22
VOLUME /geocall_config/geocall/conf
ADD applicazione/ergpi.war /apache-tomcat-8.0.35/webapps/
ENV CONF_LOCATION /geocall_config/geocall/conf
ADD applicazione/ergpi_prod.xml /geocall_config/geocall/conf/
    
EXPOSE 8080 8443

#CMD ["/usr/local/tomcat/startup.sh","run"]
CMD ["/apache-tomcat-8.0.35/bin/catalina.sh","run"]